# Samsung Tab A6 (SM-T580)

## Contents

|          File           |                    Description                     |
|-------------------------|----------------------------------------------------|
| `nvm_tlv_tf_1.1.bin`    | Bluetooth firmware for Qualcomm qca6174            |
