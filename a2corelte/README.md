# Samsung Galaxy A2 Core

## Contents

|          File           |                    Description                     |
|-------------------------|----------------------------------------------------|
| `bluetooth_bcm.hcd`     | Bluetooth firmware for Broadcom BCM43430B0         |
| `wlansdio_bcm.bin`      | Wi-Fi SDIO firmware for Broadcom BCM43430B0        |
| `wlansdio_bcm.clm_blob` | Wi-Fi SDIO regulatory data for Broadcom BCM43430B0 |
| `wlansdio_bcm.txt`      | Wi-Fi SDIO configuration for Broadcom BCM43430B0   |
